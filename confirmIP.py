#!/usr/bin/env python

import socket
import logging
import sys, getopt, time

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *

debug = True
MESSAGE = "GET %s HTTP/1.1" + "\x0d\x0a" + "Host: %s" + "\x0d\x0a\x0d\x0a"
port = 80
inputfile = ""

hostnames = []
hosts = sys.stdin.readlines()
for addr in hosts:
	hostnames.append(addr.rstrip("\n"))

f = open("carrierConfirmed",'w')
for host in hostnames:
	# first we create a real handshake and send the censored term
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	# why 5 seconds?  idk you got a better idea?
	s.settimeout(3)

	# make sure we can resolve the host
	try:
		ipaddr = socket.gethostbyname(host)
	except socket.gaierror:
		print 'fail ' + host
		continue

	# make sure the host is up
	try:
		s.connect((ipaddr, port))
	except socket.timeout:
		print 'fail ' + host
		continue 
	except socket.error:
		print 'fail ' + host
		continue
	s.send(MESSAGE % ("/", host))
	
	try:
		response = s.recv(1024)
	except socket.timeout:
		print 'fail ' + host
		continue
	except socket.error:
		print 'fail ' + host
		continue

	s.close()

	# TODO: implement other valid response codes, this is a hack.
	if response.find("HTTP/1.1") != -1:
		f.write(host + '\n')
		print host
f.close()
#if __name__ == "__main__":
