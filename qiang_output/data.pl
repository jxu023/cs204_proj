use strict;
use warnings;


#outputs:
#value dst ip, 

#early terminations
#hops past border
#number of IDS found
#number of terminated routes
#list of IDS

#hops frequency
#ASN+company frequency
my $filename = 'temp.txt';
my $fh;
my $file;

my $prev;
my $prev_ip;
my $line;
my $hops = 0;
my @ips_before_none = ();
my %hopsmap;
my %asnmap;
my %ids_ips;
my $silent_ids_count = 0;
my $total = 0;

sub reset_ip {
    #close temp.txt
    #reset border hops
    #clear arrays
    #reopen temp.txt
    $hops = 0;
    close $file;
    #truncate './temp.txt', 0;
    @ips_before_none = ();
    open($file, '>', $filename) or die 'can\'t open';
    print $file "begin\n";
}

sub run_cymru {
    my @args = ('./cymru.sh');
    system(@args) == 0 or die 'cymru failed';
    open($fh, '<', 'temp2') or die 'can\'t open';
    my $afterUS = 0;
    my $prev_line2;
    while (my $line2 = <$fh>) {
        #print $line2;
        if ($line2 =~ /US$/) {
            $afterUS = 1;
        }
        elsif ($line2 =~ /CN$/) {
            ++$hops;
        }
        elsif ($line2 =~ /NA$/) {
            if ($afterUS == 1) {
                ++$hops;
            }
        }
        if ($line2 =~ /([0-9]+)\s+\|.*?\|( .*?)\,/) {
            $prev_line2 = $line2;
        }
    }   
    return $prev_line2;
}

#reset_ip();
open($file, '>', $filename) or die 'can\'t open';
while ($line = <STDIN>) {
    if ($line =~ /IDS_here/) {
        print $file "end\n";
        chomp $prev_ip;
        $ids_ips{$prev_ip}++;
        if ($prev =~ /None/) {
            ++$silent_ids_count;
        }
        my $line2 = run_cymru();
        if ($line2 =~ /([0-9]+)\s+\|.*?\|( .*?)\,/) {
            #print "asn:$1 company: $2\n";
            my $mykey = $1 . $2;
            push @{$asnmap{$mykey}}, $prev_ip;
        }
        push @{$hopsmap{$hops}}, $prev_ip;
        #print "IDS: $prev_ip was $hops away from the border\n";
        close $fh;
        #creymu for country has US, CN, or NA (not available)
        #then count border hops
        #record previous ip as IDS, may be none
        reset_ip();
    } elsif ($line =~ /^\s+$/) {
        reset_ip();
    } elsif ($line =~ /[0-9]/) {
        #add ip to temp.txt
        print $file $line;
        $prev = $line;
        $prev_ip = $line;
    } elsif ($line =~ /None/) {
        #need to factor this into hops past border count
        $hops = $hops + 1; #if previous ip was past US ...
        push @ips_before_none, $prev_ip;
        $prev = $line;
    }
    #print $line;
}
close $file;
my $fh2;
open($fh2,'>','final_results');
foreach my $key (keys %hopsmap){
    my $len = scalar(@{$hopsmap{$key}});
    print "key:$key length:$len\n";
    print $fh2 "key:$key length:$len\n";
    for my $elem (@{$hopsmap{$key}}) {
        print $fh2 $elem , '\n';
    }
    print $fh2 "\n";
}
print $fh2 "****************asn\n";
foreach my $key (keys %asnmap){
    my $len = scalar(@{$asnmap{$key}});
    print "key:$key length:$len\n";
    print $fh2 "key:$key length:$len\n";
    for my $elem (@{$asnmap{$key}}) {
        print $fh2 $elem , '\n';
    }
    print $fh2 "\n";
}
print "silent_ids_count:$silent_ids_count\n";
my $numids = 0;
my $numtot = 0;
foreach my $key (keys %ids_ips) {
    ++$numids;
    $numtot += $ids_ips{$key};
    print $fh2 $key , "\n";
}
close($fh2);
print "uniq_total_ids:$numids\n";
print "dup_total_ids:$numtot\n";
