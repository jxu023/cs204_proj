#!/usr/bin/env python

import socket
import logging
import sys, getopt, time

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *

debug = True
MESSAGE = "GET %s HTTP/1.1" + "\x0d\x0a" + "Host: %s" + "\x0d\x0a\x0d\x0a"
port = 80
inputfile = ""
outputfile = "output.txt"

hostnames = []
hosts = sys.stdin.readlines()
confirmed_hosts = []
for addr in hosts:
	hostnames.append(addr.rstrip("\n"))

for host in hostnames:
	print 'testing ' + host + ' now'
	# first we create a real handshake and send the censored term
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	# why 5 seconds?  idk you got a better idea?
	s.settimeout(5)

	# make sure we can resolve the host
	try:
		ipaddr = socket.gethostbyname(host)
	except socket.gaierror:
		print "Could not resolve " + host
		continue

	# make sure the host is up
	try:
		s.connect((ipaddr, port))
	except socket.timeout:
		print "connection to " + host + " has timed out moving on"
		continue 
	except socket.error:
		print "connection failed, moving on"
		continue
	s.send(MESSAGE % ("/", host))
	
	try:
		response = s.recv(1024)
	except socket.timeout:
		print "connection to " + host + " has timedout moving on, Possibly not a webserver"
		continue
	except socket.error:
		print "RST: Possibly already blocked"
		continue

	s.close()
	print response
	# TODO: implement other valid response codes, this is a hack.
	if response.find("200 OK") != -1 or response.find("302 Redirect") != -1 or response.find("401 Unauthorized") != -1 or response.find("403 Forbidden") != -1:
		print host + ' is valid'
		confirmed_hosts.append(host)
	else:
		print response

for ip in confirmed_hosts:
	sys.stdout.write(ip + "\n")
#if __name__ == "__main__":
